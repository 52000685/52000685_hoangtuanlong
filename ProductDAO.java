package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Connection.ConnData;
import model.Product;

public class ProductDAO implements Repository<Product, Integer>{

	@Override
	public Integer add(Product item) {
		// TODO Auto-generated method stub
				try {
					//Bước 1: Tạo kết nối
					Connection connection=ConnData.getConnection();
					//Bước 2: Tạo đối tượng statement
					Statement st=connection.createStatement();
					//Bước 3: Thực thi câu lênh SQL
					String sql="INSERT INTO sach(id, ten, gia)"
							+ "VALUES ('" + item.getId() + "' , '" + item.getTen() + "' , " + item.getGia()  +")";
					
					int ketqua =st.executeUpdate(sql);
					
					//Bước 4: xử lý kết quả
					System.out.println("Ban da thuc thi : "+sql);
					System.out.println("So dong vua them: "+ketqua );
					
					//Bước 5 ngắt kết nối
					ConnData.closeConnection(connection);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return 0;
	}


	public Product read(Integer id) {
		Product pro = null;
		try {
			Connection connection = ConnData.getConnection();
			
			String sql = "select * from sach where id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				pro = new Product();
				pro.setId(resultSet.getInt("id"));
				pro.setTen(resultSet.getString("ten"));
				pro.setGia(resultSet.getFloat("id"));

			}
			ConnData.closeConnection(connection);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return pro;
	}

	@Override
	public boolean update(Product item) {
		// TODO Auto-generated method stub
		try {
			Connection connection = ConnData.getConnection();
			
			String sql = "update sach set ten =?, gia=? where id=?";
			
			PreparedStatement statement = connection.prepareStatement(sql);

			statement.setString(1, item.getTen());
			statement.setDouble(2, item.getGia());
			statement.setInt(3, item.getId());
			
			int result = statement.executeUpdate();
			
			System.out.println("Ban da thuc thi : "+sql);
			System.out.println("So dong vua them: "+result );
			
			ConnData.closeConnection(connection);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(Integer id) {
		try {
			//Bước 1: Tạo kết nối
			Connection connection=ConnData.getConnection();
			
		
			//Bước 3: Thực thi câu lênh SQL
			String query = "delete from sach where id = ?";

			//Bước 2: Tạo đối tượng statement
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, id);

			
			int ketqua = pstmt.executeUpdate();
			
			//Bước 4: xử lý kết quả
			System.out.println("Ban da thuc thi : "+query);
			System.out.println("So dong vua xoa: "+ketqua );
			
			//Bước 5 ngắt kết nối
			ConnData.closeConnection(connection);
				
		}catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}


	@Override
	public List<Product> readAll() {
		ArrayList<Product> studentList = new ArrayList<Product>();
		try {
			//Bước 1: Tạo kết nối
			Connection connection=ConnData.getConnection();
			Statement st = connection.createStatement();
			String sql = "select * from sach";
			ResultSet resuluSet = st.executeQuery(sql);
			
			
			System.out.println("Ban da thuc thi : "+sql);
			System.out.println("Danh sach san pham");
			while(resuluSet.next()) {
				int id = resuluSet.getInt("id");
				String ten = resuluSet.getString("ten");
				float gia = resuluSet.getFloat("gia");
				Product pro = new Product(id,ten,gia);
				studentList.add(pro);
			}
			ConnData.closeConnection(connection);			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return studentList;
	}


	public static ProductDAO getInstance() {
		// TODO Auto-generated method stub
		return new ProductDAO();
	}


}
