package model;

public class Product {
	Integer id;
	String ten;
	float gia;
	public Product() {
	
	}
	public Product(String ten, float gia) {
		super();
		this.id = 0;
		this.ten = ten;
		this.gia = gia;
	}
	public Product(Integer id, String ten, float gia) {
		super();
		this.id = id;
		this.ten = ten;
		this.gia = gia;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public float getGia() {
		return gia;
	}

	public void setGia(float gia) {
		this.gia = gia;
	}

	public String toString() {
		String result = this.getId() + "," + this.getTen() + "," + this.getGia();
		return result;
	}
	
}
