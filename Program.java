package Test;

import java.util.List;
import java.util.Scanner;

import DAO.ProductDAO;
import model.Product;

public class Program {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		menu();
	}
	public static void add(Product st) {
		ProductDAO.getInstance().add(st);
	}
	
	public static void delete(int id) {
		ProductDAO.getInstance().delete(id);
	}
	
	public static void update(Product st) {
		ProductDAO.getInstance().update(st);
	}
	
	public static void getAllProduct() {
		List<Product> studentList = ProductDAO.getInstance().readAll();
		for(Product student : studentList) {
			System.out.println(student);
		}	
	}
	
	public static void getProductByid(Integer id) {
		Product pro = ProductDAO.getInstance().read(id);
		System.out.println(pro);	
	}
	
	public static void menu() {

		int choice = 1;
		do {
	        System.out.println("********************MENU********************");
	        System.out.println("1. Read all product.");
	        System.out.println("2. Read detail of a product by id.");
	        System.out.println("3. Add a new product.");
	        System.out.println("4. Update a product.");
	        System.out.println("5. Delete a product by id.");
	        System.out.println("0. Thoat.");
	        System.out.println("********************************************");
	        Scanner sc = new Scanner(System.in);
			System.out.println("Nhap tuy chon.");
			choice = sc.nextInt();
			
			Integer id;
			String ten;
			float gia;
			Product pro;
			switch(choice) {
				case 1:
					getAllProduct();
				break;
				case 2:
	        		System.out.println("Nhap id sach can tim: ");
	        		id = sc.nextInt();
	        		getProductByid(id);
				break;
				case 3:
					System.out.println("Nhap ten sach: ");
					ten = sc.nextLine();
					
					ten = sc.nextLine();
					System.out.println("Nhap gia sach: ");
					gia = sc.nextFloat();
						
					pro = new Product(ten,gia);
					add(pro);
					break;
				case 4:
	        		System.out.println("Nhap id sach can thay doi: ");
	        		id = sc.nextInt();
	        		System.out.println("Nhap ten sach: ");
	        		ten = sc.next();
	        		System.out.println("Nhap gia sach: ");
	        		gia = sc.nextFloat();
	        		pro = new Product(id,ten,gia);
	        		update(pro);
				break;
				case 5:
        		System.out.println("Nhap id sach can xoa: ");
        		id = sc.nextInt();
        		delete(id);
					break;
				default:
					System.exit(0);
						
				}
		}while(choice!=6);
	}

	
}
