package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnData {
	public static Connection getConnection() {
		Connection connection = null;

		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			String url = "jdbc:mysql://localhost:3306/sachmn";
			String user = "root";
			String password = "";
			connection = DriverManager.getConnection(url,user,password);
		}catch(Exception ex) {
			ex.printStackTrace();
		}	
		return connection;
	}
	public static void closeConnection(Connection connection) {
		try {
			if(connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
